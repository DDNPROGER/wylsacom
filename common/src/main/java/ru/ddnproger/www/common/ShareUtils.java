package ru.ddnproger.www.common;

import android.content.Intent;
import android.net.Uri;

/**
 * Created by DDN on 11.04.2017.
 */

public class ShareUtils extends UtilsManager {

    public static void openLink(String pUrl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse(pUrl));
        getContext().startActivity(intent);
    }
}
