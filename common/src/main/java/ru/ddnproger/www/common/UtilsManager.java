package ru.ddnproger.www.common;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class UtilsManager {

    private static Context sContext;

    public static void onCreate(Context pContext) {
        sContext = pContext;
    }

    public static Context getContext() {
        return sContext;
    }

    public static String getString(@StringRes int pId) {
        return sContext.getString(pId);
    }

    public static String getString(@StringRes int pId, Object... pFormatArgs) {
        return sContext.getString(pId, pFormatArgs);
    }

    public static int getColor(@ColorRes int pId) {
        return sContext.getResources().getColor(pId);
    }

    public static Resources getResources() {
        return sContext.getResources();
    }

    public static float getDimension(@DimenRes int pId) {
        return sContext.getResources().getDimension(pId);
    }

    public static int getColorFromRes(int pId) {
        return ContextCompat.getColor(sContext, pId);
    }

    public static String getDate(long pTimeStamp, String pFormat) {
        try {
            DateFormat sdf = new SimpleDateFormat(pFormat);
            return sdf.format((new Date(pTimeStamp * 1000L)));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
