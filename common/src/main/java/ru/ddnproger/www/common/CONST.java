package ru.ddnproger.www.common;

/**
 * Created by Dmitry on 06.04.2017.
 */

public interface CONST {

    int
        NOT_DEFINED = -1;

    String
            EXTRA_VIDEO     = "EXTRA_VIDEO",
            EXTRA_ARTICLE   = "EXTRA_ARTICLE",
            EXTRA_REVIEW    = "EXTRA_REVIEW",
            EXTRA_NEWS      = "EXTRA_NEWS",
            EXTRA_AUTO      = "EXTRA_AUTO",
            KEY_VIDEO_ID    = "KEY_VIDEO_ID",
            CONTENT_TYPE    = "CONTENT_TYPE",

            WYLSA_ARTICLES_URL  = "https://wylsa.com/category/articles/",
            WYLSA_AUTOS_URL     = "https://wylsa.com/category/%D0%B0%D0%B2%D1%82%D0%BE/",
            WYLSA_REVIEWS_URL   = "https://wylsa.com/category/reviews/",
            WYLSA_NEWSES_URL    = "https://wylsa.com/category/news/",
            WYLSA_INSTA_URL     = "https://www.instagram.com/wylsacom/",
            WYLSA_YOUTUBE_URL   = "https://www.youtube.com/user/Wylsacom";

    String
            KEY_YOUTUBE     = "AIzaSyBjvgjaug-6cjewWzH-tZwKNUGsGx-ryJs";
}
