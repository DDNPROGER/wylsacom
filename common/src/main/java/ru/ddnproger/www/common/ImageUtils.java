package ru.ddnproger.www.common;

import android.support.annotation.IdRes;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Created by Yagupov Ruslan on 12.08.2016.
 */
public class ImageUtils extends UtilsManager {
    
    public static RequestCreator load(String pUrl) {
        return Picasso.with(getContext()).load(pUrl);
    }

    public static RequestCreator load(@IdRes int pId) {
        return Picasso.with(getContext()).load(pId);
    }
}
