package ru.ddnproger.www.model.providers.impl;

import android.content.Context;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class BaseProvider {

    private Context mContext;

    public BaseProvider(Context pContext) {
        mContext = pContext;
    }

    public Context getContext() {
        return mContext;
    }
}
