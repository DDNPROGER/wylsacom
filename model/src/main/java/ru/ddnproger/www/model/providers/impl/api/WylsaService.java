package ru.ddnproger.www.model.providers.impl.api;


import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;

/**
 * Created by Dmitry on 07.04.2017.
 */

public interface WylsaService {

    @Streaming
    @GET("articles.php")
    Observable<Articles> getArticles(@Query("page") int pPage);
    @Streaming
    @GET("article_detail.php")
    Observable<Article> getArticle(@Query("url") String pUrl);

    @Streaming
    @GET("auto.php")
    Observable<Autos> getAutos(@Query("page") int pPage);
    @Streaming
    @GET("auto_detail.php")
    Observable<Auto> getAuto(@Query("url") String pUrl);

    @Streaming
    @GET("news.php")
    Observable<Newses> getNewses(@Query("page") int pPage);
    @Streaming
    @GET("news_detail.php")
    Observable<News> getNews(@Query("url") String pUrl);

    @Streaming
    @GET("reviews.php")
    Observable<Reviews> getReviews(@Query("page") int pPage);
    @GET("reviews_detail.php")
    Observable<Review> getReview(@Query("url") String pUrl);

}
