package ru.ddnproger.www.model.providers.exception;

import android.support.annotation.StringRes;

import ru.ddnproger.www.model.ModelManager;

/**
 * Created by Yagupov Ruslan on 10.08.2016.
 */
abstract class BaseException extends RuntimeException {

    private @StringRes
    int mMessageId;


    BaseException(@StringRes int pMessageId) {
        mMessageId = pMessageId;
    }

    @Override
    public String getLocalizedMessage() {
        return ModelManager.getContext().getString(mMessageId);
    }
}
