package ru.ddnproger.www.model.providers.impl.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.R;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.InstaPosts;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;
import ru.ddnproger.www.model.models.YoutubeVideos;
import ru.ddnproger.www.model.providers.RemoteProvider;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.model.providers.impl.BaseProvider;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class RemoteProviderImpl extends BaseProvider implements RemoteProvider {

    private OkHttpClient.Builder mHttpClient = new OkHttpClient.Builder();
    private WylsaService API_SERVICE = createService(WylsaService.class, ApiUrl.getApiUrl());
    private YoutubeService YOUTUBE_SERVICE = createService(YoutubeService.class, ApiUrl.getYoutubeUrl());
    private InstaService INSTA_SERVICE = createService(InstaService.class, ApiUrl.getInstaUrl());

    public RemoteProviderImpl(Context pContext) {
        super(pContext);
    }

    private <S> S createService(Class<S> pServiceClass, String pBaseUrl){
        addInternetAvailableException();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        mHttpClient.addInterceptor(loggingInterceptor);

        return new Retrofit.Builder()
                .baseUrl(pBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(mHttpClient.build())
                .build()
                .create(pServiceClass);
    }

    @Override
    public boolean isRemoteProviderAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public Observable<Articles> getArticles(int pPage) {
        return API_SERVICE.getArticles(pPage);
    }

    @Override
    public Observable<Article> getArticle(String pUrl) {
        return API_SERVICE.getArticle(pUrl);
    }

    @Override
    public Observable<Autos> getAutos(int pPage) {
        return API_SERVICE.getAutos(pPage);
    }

    @Override
    public Observable<Auto> getAuto(String pUrl) {
        return API_SERVICE.getAuto(pUrl);
    }

    @Override
    public Observable<Newses> getNewses(int pPage) {
        return API_SERVICE.getNewses(pPage);
    }

    @Override
    public Observable<News> getNews(String pUrl) {
        return API_SERVICE.getNews(pUrl);
    }

    @Override
    public Observable<Reviews> getReviews(int pPage) {
        return API_SERVICE.getReviews(pPage);
    }

    @Override
    public Observable<Review> getReview(String pUrl) {
        return API_SERVICE.getReview(pUrl);
    }

    @Override
    public Observable<YoutubeVideos> getYoutubeVideos(String nextPageToken) {
        return YOUTUBE_SERVICE.getYoutubeVideos(CONST.KEY_YOUTUBE, nextPageToken);
    }

    @Override
    public Observable<InstaPosts> getInstaPosts(String pId) {
        return INSTA_SERVICE.getInstaPosts(pId);
    }

    private void addInternetAvailableException() {
        mHttpClient.addInterceptor(chain -> {
            if (!isRemoteProviderAvailable())
                throw new InternetException(R.string.check_internet_connection);
            return chain.proceed(chain.request());
        });
    }
}
