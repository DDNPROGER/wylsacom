package ru.ddnproger.www.model.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;

import ru.ddnproger.www.model.providers.impl.api.ApiUrl;

/**
 * Created by Dmitry on 10.04.2017.
 */

public class InstaPost implements Serializable {
    private Likes likes;
    private String id;
    private Map<String, InstaImage> images;
    private String link;
    private Caption caption;

    public Likes getLikes() {
        return likes;
    }

    public String getId() {
        return id;
    }

    public Map<String, InstaImage> getImages() {
        return images;
    }

    public class Likes implements Serializable {
        private int count;

        public int getCount() {
            return count;
        }
    }

    public class InstaImage implements Serializable {
        private int width;
        private int height;
        private String url;

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public String getUrl() {
            return url;
        }
    }

    public class Caption implements Serializable {
        @SerializedName("created_time")
        String createdTime;
        String text;
        String id;

        public String getCreatedTime() {
            return createdTime;
        }

        public String getText() {
            return text;
        }

        public String getId() {
            return id;
        }
    }

    public String getImgUrl(){
        return images.get("standard_resolution").url.replace("480x480", "1080x1080");
    }

    public String getPreviewText(){
        return caption.text;
    }

    public String getLink(){
        return link;
    }
}
