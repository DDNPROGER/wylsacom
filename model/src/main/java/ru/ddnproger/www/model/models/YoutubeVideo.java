package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by DDN on 10.04.2017.
 */

public class YoutubeVideo implements Serializable {

    private Snippet snippet;
    private Id id;

    public String getImgUrl(){
        return snippet.thumbnails.get("high").getUrl();
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public Id getId() {
        return id;
    }

    public class Id implements Serializable {
        String videoId;

        public String getVideoId() {
            return videoId;
        }
    }

    public class Snippet implements Serializable {
        private String publishedAt;
        private String title;
        private String description;
        private Map<String, Thumb> thumbnails;

        public String getPublishedAt() {
            return publishedAt;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public Map<String, Thumb> getThumbnails() {
            return thumbnails;
        }
    }

    public class Thumb implements Serializable {
        String url;
        int width;
        int height;

        public String getUrl() {
            return url;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }
}
