package ru.ddnproger.www.model.models;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class Review extends RealmObject implements Serializable {

    @PrimaryKey
    private String url;
    private String date;
    private String title;
    private String description;
    private String imgUrl;
    private String author;
    private String authorLink;

    public Review() {
    }

    public Review(String pUrl, String pDate, String pTitle, String pDescription, String pImgUrl, String pAuthor, String pAuthorLink) {
        url = pUrl;
        date = pDate;
        title = pTitle;
        description = pDescription;
        imgUrl = pImgUrl;
        author = pAuthor;
        authorLink = pAuthorLink;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String pUrl) {
        url = pUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String pDate) {
        date = pDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String pTitle) {
        title = pTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String pImgUrl) {
        imgUrl = pImgUrl;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String pAuthor) {
        author = pAuthor;
    }

    public String getAuthorLink() {
        return authorLink;
    }

    public void setAuthorLink(String pAuthorLink) {
        authorLink = pAuthorLink;
    }

    public String getFullAuthor() {
        return "<a href=\"" + authorLink + "\">" + author + "</a>";
    }
}
