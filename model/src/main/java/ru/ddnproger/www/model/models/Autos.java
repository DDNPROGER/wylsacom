package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class Autos implements Serializable {

    private List<Auto> data;

    public List<Auto> getData() {
        return data;
    }

    public Autos(List<Auto> pData) {
        data = pData;
    }
}
