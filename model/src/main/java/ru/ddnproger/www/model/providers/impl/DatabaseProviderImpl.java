package ru.ddnproger.www.model.providers.impl;

import android.content.Context;

import java.util.List;

import io.reactivex.Observable;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.annotations.RealmModule;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;
import ru.ddnproger.www.model.providers.DatabaseProvider;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class DatabaseProviderImpl extends BaseProvider implements DatabaseProvider {

    private static final int DATABASE_VERSION = 0;
    private static final String DATABASE_NAME = "wylsacom.realm";
    private static final String FIELD_ID = "url";

    public DatabaseProviderImpl(Context pContext) {
        super(pContext);
        initRealmConfig(pContext);
    }

    @Override
    public Observable<Articles> getArticles(int pPage) {
        Realm realm = getRealm();
        List<Article> articles = (realm.copyFromRealm(realm.where(Article.class).findAll()));
        realm.close();
        return Observable.just(new Articles(articles));
    }

    @Override
    public Observable<Article> getArticle(String pUrl) {
        Realm realm = getRealm();
        Article article = (realm.copyFromRealm(realm.where(Article.class).equalTo(FIELD_ID, pUrl).findFirst()));
        realm.close();
        return Observable.just(article);
    }

    @Override
    public void saveArticle(Article pArticle) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> db.insertOrUpdate(pArticle));
        realm.close();
    }

    @Override
    public void saveArticles(Articles pArticles) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> {
            for (Article article : pArticles.getData()) db.insertOrUpdate(article);
        });
        realm.close();
    }

    @Override
    public Observable<Reviews> getReviews(int pPage) {
        Realm realm = getRealm();
        List<Review> reviews = (realm.copyFromRealm(realm.where(Review.class).findAll()));
        realm.close();
        return Observable.just(new Reviews(reviews));
    }

    @Override
    public Observable<Review> getReview(String pUrl) {
        Realm realm = getRealm();
        Review review = (realm.copyFromRealm(realm.where(Review.class).equalTo(FIELD_ID, pUrl).findFirst()));
        realm.close();
        return Observable.just(review);
    }

    @Override
    public void saveReview(Review pReview) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> db.insertOrUpdate(pReview));
        realm.close();
    }

    @Override
    public void saveReviews(Reviews pReviews) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> {
            for (Review review : pReviews.getData()) db.insertOrUpdate(review);
        });
        realm.close();
    }

    @Override
    public Observable<Autos> getAutos(int pPage) {
        Realm realm = getRealm();
        List<Auto> autos = (realm.copyFromRealm(realm.where(Auto.class).findAll()));
        realm.close();
        return Observable.just(new Autos(autos));
    }

    @Override
    public Observable<Auto> getAuto(String pUrl) {
        Realm realm = getRealm();
        Auto auto = (realm.copyFromRealm(realm.where(Auto.class).equalTo(FIELD_ID, pUrl).findFirst()));
        realm.close();
        return Observable.just(auto);
    }

    @Override
    public void saveAuto(Auto pAuto) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> db.insertOrUpdate(pAuto));
        realm.close();
    }

    @Override
    public void saveAutos(Autos pAutos) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> {
            for (Auto auto : pAutos.getData()) db.insertOrUpdate(auto);
        });
        realm.close();
    }

    @Override
    public Observable<Newses> getNewses(int pPage) {
        Realm realm = getRealm();
        List<News> newses = (realm.copyFromRealm(realm.where(News.class).findAll()));
        realm.close();
        return Observable.just(new Newses(newses));
    }

    @Override
    public Observable<News> getNews(String pUrl) {
        Realm realm = getRealm();
        News news = (realm.copyFromRealm(realm.where(News.class).equalTo(FIELD_ID, pUrl).findFirst()));
        realm.close();
        return Observable.just(news);
    }

    @Override
    public void saveNews(News pNews) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> db.insertOrUpdate(pNews));
        realm.close();
    }

    @Override
    public void saveNewses(Newses pNewses) {
        Realm realm = getRealm();
        realm.executeTransaction(db -> {
            for (News news : pNewses.getData()) db.insertOrUpdate(news);
        });
        realm.close();
    }

    @Override
    public boolean isArticlesAvailable() {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Article.class).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isReviewsAvailable() {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Review.class).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isAutosAvailable() {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Auto.class).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isNewsesAvailable() {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(News.class).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isArticleAvailable(String pUrl) {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Article.class).equalTo(FIELD_ID, pUrl).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isAutoAvailable(String pUrl) {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Auto.class).equalTo(FIELD_ID, pUrl).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isReviewAvailable(String pUrl) {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(Review.class).equalTo(FIELD_ID, pUrl).count() > 0;
        realm.close();
        return isAvailable;
    }

    @Override
    public boolean isNewsAvailable(String pUrl) {
        Realm realm = getRealm();
        boolean isAvailable = realm.where(News.class).equalTo(FIELD_ID, pUrl).count() > 0;
        realm.close();
        return isAvailable;
    }

    private void initRealmConfig(Context pContext) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(pContext)
                .name(DATABASE_NAME)
                .schemaVersion(DATABASE_VERSION)
                .modules(new RealmDatabaseModule())
                .migration(new Migration())
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private Realm getRealm() {
        return Realm.getDefaultInstance();
    }

    @RealmModule(classes = {Article.class, Auto.class, News.class, Review.class})
    private class RealmDatabaseModule {
    }

    private class Migration implements RealmMigration {

        @Override
        public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        }
    }
}
