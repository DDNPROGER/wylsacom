package ru.ddnproger.www.model.providers.impl.api;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import ru.ddnproger.www.model.models.YoutubeVideos;

/**
 * Created by DDN on 10.04.2017.
 */

public interface YoutubeService {

    @Streaming
    @GET("search?&part=snippet,id&channelId=UCt7sv-NKh44rHAEb-qCCxvA&order=date&maxResults=20")
    Observable<YoutubeVideos> getYoutubeVideos(@Query("key") String pKey, @Query("pageToken") String pNextPage);
}
