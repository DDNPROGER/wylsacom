package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class Newses implements Serializable {

    private List<News> data;

    public List<News> getData() {
        return data;
    }

    public Newses(List<News> pData) {
        data = pData;
    }
}
