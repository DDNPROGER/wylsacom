package ru.ddnproger.www.model.providers.impl.api;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import ru.ddnproger.www.model.models.InstaPosts;

/**
 * Created by Dmitry on 10.04.2017.
 */

public interface InstaService {

    @Streaming
    @GET("wylsacom/media/")
    Observable<InstaPosts> getInstaPosts(@Query("max_id") String maxId);
}
