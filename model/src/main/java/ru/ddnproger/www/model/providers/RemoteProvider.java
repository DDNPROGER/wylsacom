package ru.ddnproger.www.model.providers;

import io.reactivex.Observable;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.InstaPosts;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;
import ru.ddnproger.www.model.models.YoutubeVideos;

/**
 * Created by Dmitry on 07.04.2017.
 */

public interface RemoteProvider {

    boolean isRemoteProviderAvailable();

    Observable<Articles> getArticles(int pPage);
    Observable<Article> getArticle(String pUrl);

    Observable<Autos> getAutos(int pPage);
    Observable<Auto> getAuto(String pUrl);

    Observable<Newses> getNewses(int pPage);
    Observable<News> getNews(String pUrl);

    Observable<Reviews> getReviews(int pPage);
    Observable<Review> getReview(String pUrl);

    Observable<YoutubeVideos> getYoutubeVideos(String nextPageToken);

    Observable<InstaPosts> getInstaPosts(String pId);
}
