package ru.ddnproger.www.model.providers.exception;

import android.support.annotation.StringRes;

public class InternetException extends BaseException {

    public InternetException(@StringRes int pMessageId) {
        super(pMessageId);
    }
}