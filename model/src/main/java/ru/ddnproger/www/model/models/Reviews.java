package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class Reviews implements Serializable {

    private List<Review> data;

    public List<Review> getData() {
        return data;
    }

    public Reviews(List<Review> pData) {
        data = pData;
    }
}
