package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class Articles implements Serializable{
    private List<Article> data;

    public List<Article> getData() {
        return data;
    }

    public Articles(List<Article> pData) {
        data = pData;
    }
}
