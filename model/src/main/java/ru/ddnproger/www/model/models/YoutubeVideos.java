package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DDN on 10.04.2017.
 */

public class YoutubeVideos implements Serializable {

    private String nextPageToken;
    private List<YoutubeVideo> items;

    public List<YoutubeVideo> getItems() {
        return items;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }
}
