package ru.ddnproger.www.model.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry on 10.04.2017.
 */

public class InstaPosts implements Serializable {

    private List<InstaPost> items;

    public List<InstaPost> getItems() {
        return items;
    }

    public String getLastId(){
        return items.get(items.size()-1).getId();
    }
}
