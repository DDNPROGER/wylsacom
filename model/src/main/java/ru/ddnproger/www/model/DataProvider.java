package ru.ddnproger.www.model;

import android.content.Context;

import io.reactivex.Observable;
import retrofit2.HttpException;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.InstaPosts;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;
import ru.ddnproger.www.model.models.YoutubeVideos;
import ru.ddnproger.www.model.providers.DatabaseProvider;
import ru.ddnproger.www.model.providers.RemoteProvider;
import ru.ddnproger.www.model.providers.impl.DatabaseProviderImpl;
import ru.ddnproger.www.model.providers.impl.api.RemoteProviderImpl;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class DataProvider implements DatabaseProvider, RemoteProvider {

    private static Context sContext;
    private static DataProvider sDataProvider;
    private static DatabaseProvider sDatabaseProvider;
    private static RemoteProvider sRemoteProvider;

    private boolean mIsLoadReviewsExistHttpException = false;
    private boolean mIsLoadArticlesExistHttpException = false;
    private boolean mIsLoadAutosExistHttpException = false;
    private boolean mIsLoadNewsesExistHttpException = false;

    private boolean mIsLoadReviewExistHttpException = false;
    private boolean mIsLoadArticleExistHttpException = false;
    private boolean mIsLoadAutoExistHttpException = false;
    private boolean mIsLoadNewsExistHttpException = false;

    static void initialize(Context pContext) {
        if (sDatabaseProvider == null) {
            sContext = pContext;
            sDataProvider = new DataProvider();
            sDatabaseProvider = new DatabaseProviderImpl(sContext);
            sRemoteProvider = new RemoteProviderImpl(sContext);
        } else throw new RuntimeException("Provider has initialized already");
    }

    public static DataProvider getInstance() {
        if (sDataProvider == null) throw new RuntimeException("Provider does not initialized");

        return sDataProvider;
    }

    @Override
    public boolean isRemoteProviderAvailable() {
        return sRemoteProvider.isRemoteProviderAvailable();
    }

    @Override
    public Observable<Articles> getArticles(int pPage) {
        if (isRemoteProviderAvailable() && !isLoadArticlesExistHttpException()) {
            return sRemoteProvider.getArticles(pPage);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<Article> getArticle(String pUrl) {
        if (isRemoteProviderAvailable() && !isLoadArticleExistHttpException()) {
            return sRemoteProvider.getArticle(pUrl);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public void saveArticle(Article pArticle) {
        sDatabaseProvider.saveArticle(pArticle);
    }

    @Override
    public void saveArticles(Articles pArticles) {
        sDatabaseProvider.saveArticles(pArticles);
    }

    @Override
    public Observable<Autos> getAutos(int pPage) {
        if (isRemoteProviderAvailable() && !isLoadAutosExistHttpException()) {
            return sRemoteProvider.getAutos(pPage);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<Auto> getAuto(String pUrl) {
        if (isRemoteProviderAvailable() && !isLoadAutoExistHttpException()) {
            return sRemoteProvider.getAuto(pUrl);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public void saveAuto(Auto pAuto) {
        sDatabaseProvider.saveAuto(pAuto);
    }

    @Override
    public void saveAutos(Autos pAutos) {
        sDatabaseProvider.saveAutos(pAutos);
    }

    @Override
    public Observable<Newses> getNewses(int pPage) {
        if (isRemoteProviderAvailable() && !isLoadNewsesExistHttpException()) {
            return sRemoteProvider.getNewses(pPage);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<News> getNews(String pUrl) {
        if (isRemoteProviderAvailable() && !isLoadNewsExistHttpException()) {
            return sRemoteProvider.getNews(pUrl);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public void saveNews(News pNews) {
        sDatabaseProvider.saveNews(pNews);
    }

    @Override
    public void saveNewses(Newses pNewses) {
        sDatabaseProvider.saveNewses(pNewses);
    }

    @Override
    public Observable<Reviews> getReviews(int pPage) {
        if (isRemoteProviderAvailable() && !isLoadReviewsExistHttpException()) {
            return sRemoteProvider.getReviews(pPage);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<Review> getReview(String pUrl) {
        if (isRemoteProviderAvailable() && !isLoadReviewExistHttpException()) {
            return sRemoteProvider.getReview(pUrl);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<YoutubeVideos> getYoutubeVideos(String nextPageToken) {
        if (isRemoteProviderAvailable()) {
            return sRemoteProvider.getYoutubeVideos(nextPageToken);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public Observable<InstaPosts> getInstaPosts(String pId) {
        if (isRemoteProviderAvailable()){
            return sRemoteProvider.getInstaPosts(pId);
        }

        return Observable.create(pSubscriber -> pSubscriber.onError(new RuntimeException()));
    }

    @Override
    public void saveReview(Review pReview) {
        sDatabaseProvider.saveReview(pReview);
    }

    @Override
    public void saveReviews(Reviews pReviews) {
        sDatabaseProvider.saveReviews(pReviews);
    }


    public void setLoadReviewsExistHttpException(boolean pIsLoadReviewsExistHttpException) {
        mIsLoadReviewsExistHttpException = pIsLoadReviewsExistHttpException;
    }

    public boolean isLoadReviewsExistHttpException() {
        return mIsLoadReviewsExistHttpException;
    }

    public boolean isLoadArticlesExistHttpException() {
        return mIsLoadArticlesExistHttpException;
    }

    public void setLoadArticlesExistHttpException(boolean pLoadArticlesExistHttpException) {
        mIsLoadArticlesExistHttpException = pLoadArticlesExistHttpException;
    }

    public boolean isLoadAutosExistHttpException() {
        return mIsLoadAutosExistHttpException;
    }

    public void setLoadAutosExistHttpException(boolean pLoadAutosExistHttpException) {
        mIsLoadAutosExistHttpException = pLoadAutosExistHttpException;
    }

    public boolean isLoadNewsesExistHttpException() {
        return mIsLoadNewsesExistHttpException;
    }

    public void setLoadNewsesExistHttpException(boolean pLoadNewsesExistHttpException) {
        mIsLoadNewsesExistHttpException = pLoadNewsesExistHttpException;
    }

    public boolean isLoadReviewExistHttpException() {
        return mIsLoadReviewExistHttpException;
    }

    public void setLoadReviewExistHttpException(boolean pLoadReviewExistHttpException) {
        mIsLoadReviewExistHttpException = pLoadReviewExistHttpException;
    }

    public boolean isLoadArticleExistHttpException() {
        return mIsLoadArticleExistHttpException;
    }

    public void setLoadArticleExistHttpException(boolean pLoadArticleExistHttpException) {
        mIsLoadArticleExistHttpException = pLoadArticleExistHttpException;
    }

    public boolean isLoadAutoExistHttpException() {
        return mIsLoadAutoExistHttpException;
    }

    public void setLoadAutoExistHttpException(boolean pLoadAutoExistHttpException) {
        mIsLoadAutoExistHttpException = pLoadAutoExistHttpException;
    }

    public boolean isLoadNewsExistHttpException() {
        return mIsLoadNewsExistHttpException;
    }

    public void setLoadNewsExistHttpException(boolean pLoadNewsExistHttpException) {
        mIsLoadNewsExistHttpException = pLoadNewsExistHttpException;
    }

    @Override
    public boolean isArticlesAvailable() {
        return sDatabaseProvider.isArticlesAvailable();
    }

    @Override
    public boolean isReviewsAvailable() {
        return sDatabaseProvider.isReviewsAvailable();
    }

    @Override
    public boolean isAutosAvailable() {
        return sDatabaseProvider.isAutosAvailable();
    }

    @Override
    public boolean isNewsesAvailable() {
        return sDatabaseProvider.isNewsesAvailable();
    }

    @Override
    public boolean isArticleAvailable(String pUrl) {
        return sDatabaseProvider.isArticleAvailable(pUrl);
    }

    @Override
    public boolean isAutoAvailable(String pUrl) {
        return sDatabaseProvider.isAutoAvailable(pUrl);
    }

    @Override
    public boolean isReviewAvailable(String pUrl) {
        return sDatabaseProvider.isReviewAvailable(pUrl);
    }

    @Override
    public boolean isNewsAvailable(String pUrl) {
        return sDatabaseProvider.isNewsAvailable(pUrl);
    }
}
