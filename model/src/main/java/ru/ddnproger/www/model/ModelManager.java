package ru.ddnproger.www.model;

import android.content.Context;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class ModelManager {

    private static Context sContext;

    public static void onCreate(Context pContext){
        sContext = pContext;
        DataProvider.initialize(sContext);
    }

    public static Context getContext() {
        return sContext;
    }
}
