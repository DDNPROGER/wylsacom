package ru.ddnproger.www.model.providers;

import java.util.List;

import io.reactivex.Observable;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Articles;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.Autos;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Newses;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.models.Reviews;

/**
 * Created by Dmitry on 07.04.2017.
 */

public interface DatabaseProvider {

    Observable<Articles> getArticles(int pPage);
    Observable<Article> getArticle(String pUrl);
    void saveArticle(Article pArticle);
    void saveArticles(Articles pArticles);

    Observable<Reviews> getReviews(int pPage);
    Observable<Review> getReview(String pUrl);
    void saveReview(Review pReview);
    void saveReviews(Reviews pReviews);

    Observable<Autos> getAutos(int pPage);
    Observable<Auto> getAuto(String pUrl);
    void saveAuto(Auto pAuto);
    void saveAutos(Autos pAutos);

    Observable<Newses> getNewses(int pPage);
    Observable<News> getNews(String pUrl);
    void saveNews(News pNews);
    void saveNewses(Newses pNewses);

    boolean isArticlesAvailable();
    boolean isReviewsAvailable();
    boolean isAutosAvailable();
    boolean isNewsesAvailable();

    boolean isArticleAvailable(String pUrl);
    boolean isAutoAvailable(String pUrl);
    boolean isReviewAvailable(String pUrl);
    boolean isNewsAvailable(String pUrl);
}
