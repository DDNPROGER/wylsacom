package ru.ddnproger.www.model.providers.impl.api;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class ApiUrl {

    private static final String
            SERVER_URL  = "",
            YOUTUBE_URL = "https://www.googleapis.com/youtube/v3/",
            INSTA_URL   = "https://www.instagram.com/";

    public static String getApiUrl() {
        return SERVER_URL;
    }

    public static String getYoutubeUrl() {
        return YOUTUBE_URL;
    }

    public static String getInstaUrl() {
        return INSTA_URL;
    }
}
