package ru.ddnproger.www.wylsacom.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.ReviewsAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.ReviewsPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewsView;

/**
 * Created by DDN on 07.04.2017.
 */

public class ReviewsActivity extends BaseRecyclerActivity<Review, ReviewsAdapter, ReviewsPresenter> implements ReviewsView {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_reviews;
    }

    @Override
    protected int getTitleId() {
        return R.string.reviews;
    }

    @Override
    protected ReviewsAdapter getAdapter(List<Review> pItems) {
        return new ReviewsAdapter(pItems);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reviews_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
