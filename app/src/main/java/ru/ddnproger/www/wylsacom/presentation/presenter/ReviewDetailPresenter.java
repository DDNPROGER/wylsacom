package ru.ddnproger.www.wylsacom.presentation.presenter;

/**
 * Created by DDN on 08.04.2017.
 */

public interface ReviewDetailPresenter extends BasePresenter {

    void loadReviewDetail();
    void openLink();
}
