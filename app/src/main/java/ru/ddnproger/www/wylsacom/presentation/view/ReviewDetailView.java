package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Review;

/**
 * Created by DDN on 08.04.2017.
 */

public interface ReviewDetailView extends ErrorView {

    void showReviewDetail(Review pReview);
    void showReviewDescription(Review pReview);
    void showLoading();
    void hideLoading();
}
