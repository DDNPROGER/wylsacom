package ru.ddnproger.www.wylsacom.ui.activity;

import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import butterknife.BindView;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.BaseRecyclerViewAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.BaseRecyclerPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.BaseRecyclerView;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.View.VISIBLE;

/**
 * Created by DDN on 10.04.2017.
 */

public abstract class BaseRecyclerActivity<T extends Object, A extends BaseRecyclerViewAdapter, P extends BaseRecyclerPresenter> extends BaseViewActivity<P> implements BaseRecyclerView<T>, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.list)
    RecyclerView mRecycler;
    @BindView(R.id.loading)
    View mLoadingView;
    @BindView(R.id.empty)
    View mEmpty;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onSetup() {
        setTitle(getTitleId());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initRecycler();
        initRefreshLayout();
    }

    private void initRecycler() {
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(getLayoutManager());
        mRecycler.addOnScrollListener(pr().getOnScrollListener(mRecycler.getLayoutManager()));
    }

    private void initRefreshLayout() {
        mRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary);
        mRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        if (mRecycler.getAdapter() != null) {
            ((A) mRecycler.getAdapter()).clear();
        }
        pr().refresh();
    }

    @Override
    public void restore(List pItems) {
        if (mRecycler.getAdapter() == null) {
            mRecycler.setAdapter(getAdapter(pItems));
        }
    }

    @Override
    public void showItems(List pItems, int pCount) {
        if (mRefreshLayout == null || mLoadingView == null || mRecycler == null) return;
        if (pCount == 0)
            mRecycler.removeOnScrollListener(pr().getOnScrollListener(mRecycler.getLayoutManager()));

        mRefreshLayout.setRefreshing(false);

        if (mRecycler.getAdapter() == null) {
            mRecycler.setAdapter(getAdapter(pItems));
        } else {
            A adapter = (A) mRecycler.getAdapter();
            adapter.notifyItemRangeInserted(pItems.size() - pCount, pCount);
        }

        checkEmptyList();
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    private void checkEmptyList() {
        mEmpty.setVisibility(isEmptyList() ? View.VISIBLE : View.GONE);
    }

    private boolean isEmptyList() {
        return mRecycler.getAdapter() == null || mRecycler.getAdapter().getItemCount() == 0;
    }

    @Override
    protected void onError() {
        checkEmptyList();
        mRefreshLayout.setRefreshing(false);
        mRefreshLayout.setOnRefreshListener(this);
        mLoadingView.setVisibility(View.GONE);
    }

    protected abstract
    @StringRes
    int getTitleId();

    protected abstract A getAdapter(List<T> pItems);

    protected RecyclerView.LayoutManager getLayoutManager(){
        RecyclerView.LayoutManager result;
        if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            result = new LinearLayoutManager(this);
        } else result = new GridLayoutManager(this, 2);

        return result;
    }
}
