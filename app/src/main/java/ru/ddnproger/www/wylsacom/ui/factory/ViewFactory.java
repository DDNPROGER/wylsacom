package ru.ddnproger.www.wylsacom.ui.factory;

import ru.ddnproger.www.wylsacom.presentation.view.BaseView;
import ru.ddnproger.www.wylsacom.ui.activity.ArticleDetailActivity;
import ru.ddnproger.www.wylsacom.ui.activity.ArticlesActivity;
import ru.ddnproger.www.wylsacom.ui.activity.AutoDetailActivity;
import ru.ddnproger.www.wylsacom.ui.activity.AutosActivity;
import ru.ddnproger.www.wylsacom.ui.activity.InstaPostsActivity;
import ru.ddnproger.www.wylsacom.ui.activity.NewsDetailActivity;
import ru.ddnproger.www.wylsacom.ui.activity.NewsesActivity;
import ru.ddnproger.www.wylsacom.ui.activity.ReviewDetailActivity;
import ru.ddnproger.www.wylsacom.ui.activity.ReviewsActivity;
import ru.ddnproger.www.wylsacom.ui.activity.YoutubeVideosActivity;

/**
 * Created by DDN on 07.04.2017.
 */

public class ViewFactory {

    public static final int
            REVIEWS_VIEW = 0,
            ARTICLES_VIEW = 1,
            AUTOS_VIEW = 2,
            NEWSES_VIEW = 3,
            ARTICLE_DETAIL_VIEW = 4,
            REVIEW_DETAIL_VIEW = 5,
            AUTO_DETAIL_VIEW = 6,
            NEWS_DETAIL_VIEW = 7,
            YOUTUBE_VIEW = 8,
            INSTA_POSTS_VIEW = 9;

    public static BaseView create(int pViewId) {
        switch (pViewId) {
            case REVIEWS_VIEW:
                return new ReviewsActivity();
            case ARTICLES_VIEW:
                return new ArticlesActivity();
            case AUTOS_VIEW:
                return new AutosActivity();
            case NEWSES_VIEW:
                return new NewsesActivity();
            case ARTICLE_DETAIL_VIEW:
                return new ArticleDetailActivity();
            case REVIEW_DETAIL_VIEW:
                return new ReviewDetailActivity();
            case AUTO_DETAIL_VIEW:
                return new AutoDetailActivity();
            case NEWS_DETAIL_VIEW:
                return new NewsDetailActivity();
            case YOUTUBE_VIEW:
                return new YoutubeVideosActivity();
            case INSTA_POSTS_VIEW:
                return new InstaPostsActivity();
            default:
                throw new RuntimeException("Unknown view id");
        }
    }
}
