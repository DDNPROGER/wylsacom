package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.ArticleDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ArticleDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class ArticleDetailPresenterImpl extends BasePresenterImpl<ArticleDetailView> implements ArticleDetailPresenter {

    private Article mArticle;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public ArticleDetailPresenterImpl(ArticleDetailView pView) {
        super(pView);
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {
        if (pArguments.containsKey(CONST.EXTRA_ARTICLE)) {
            mArticle = (Article) pArguments.getSerializable(CONST.EXTRA_ARTICLE);
            view().showArticleDetail(mArticle);
            loadArticleDetail();
        } else {
            view().showArticleDetail(mArticle);
            view().showArticleDescription(mArticle);
        }
    }

    @Override
    public void loadArticleDetail() {
        view().showLoading();
        mDisposable.add(DataProvider.getInstance().getArticle(mArticle.getUrl())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pArticle -> {
                    mArticle.setAuthor(pArticle.getAuthor());
                    mArticle.setAuthorLink(pArticle.getAuthorLink());
                    mArticle.setDescription(pArticle.getDescription());
                    view().hideLoading();
                    view().showArticleDescription(mArticle);
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    if (pThrowable instanceof HttpException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(mArticle.getUrl());
    }
}
