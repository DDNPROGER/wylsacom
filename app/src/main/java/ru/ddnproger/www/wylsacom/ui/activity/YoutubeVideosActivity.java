package ru.ddnproger.www.wylsacom.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.models.YoutubeVideo;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.VideoAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.YoutubeVideosPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.YoutubeVideosView;

/**
 * Created by DDN on 10.04.2017.
 */

public class YoutubeVideosActivity extends BaseRecyclerActivity<YoutubeVideo, VideoAdapter, YoutubeVideosPresenter> implements YoutubeVideosView {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_youtube_videos;
    }

    @Override
    protected int getTitleId() {
        return R.string.youtube;
    }

    @Override
    protected VideoAdapter getAdapter(List<YoutubeVideo> pItems) {
        return new VideoAdapter(pItems, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.youtube_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
