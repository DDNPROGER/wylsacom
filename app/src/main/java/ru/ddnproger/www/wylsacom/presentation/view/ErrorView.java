package ru.ddnproger.www.wylsacom.presentation.view;

import android.support.annotation.StringRes;

/**
 * Created by Dmitry on 06.04.2017.
 */

public interface ErrorView extends BaseView {

    void showError(@StringRes int pErrorStringId);
    void showError(String pErrorString);
}
