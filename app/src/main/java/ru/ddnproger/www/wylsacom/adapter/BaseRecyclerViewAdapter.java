package ru.ddnproger.www.wylsacom.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.ddnproger.www.wylsacom.ui.viewholder.BaseViewHolder;


public abstract class BaseRecyclerViewAdapter<T extends List, H extends BaseViewHolder> extends RecyclerView.Adapter<H> {

    private T mItem;

    BaseRecyclerViewAdapter(T pItem) {
        mItem = pItem;
    }

    @Override
    public H onCreateViewHolder(ViewGroup parent, int viewType) {
        return createViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(getConvertViewId(), parent, false), viewType);
    }

    protected T item() {
        return mItem;
    }

    public void addAll(T pItem) {
        mItem.addAll(pItem);
        notifyDataSetChanged();
    }

    public void clear() {
        mItem.clear();
        notifyDataSetChanged();
    }


    protected abstract int getConvertViewId();

    protected abstract H createViewHolder(View pView, int pType);
}
