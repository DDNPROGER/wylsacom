package ru.ddnproger.www.wylsacom.presentation.presenter.manager;

import android.os.Bundle;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;


/**
 * Created by Dmitry on 06.04.2017.
 */

public class PresenterManager {

    private static final String KEY_PRESENTER_ID = "KEY_PRESENTER_ID";
    private static PresenterManager sInstance;

    private final AtomicLong mCurrentId;
    private final Cache<Long, BasePresenter> mPresenters;

    private PresenterManager(long pMaxSize, long pExpirationValue, TimeUnit pExpirationUnit) {
        mCurrentId = new AtomicLong();
        mPresenters = CacheBuilder.newBuilder()
                .maximumSize(pMaxSize)
                .expireAfterWrite(pExpirationValue, pExpirationUnit)
                .build();
    }

    public static PresenterManager getInstance() {
        if (sInstance == null) sInstance = new PresenterManager(10, 30, TimeUnit.MINUTES);
        return sInstance;
    }

    public void savePresenter(BasePresenter pPresenter, Bundle pOutState) {
        if (pPresenter == null) return;

        long id = mCurrentId.incrementAndGet();
        mPresenters.put(id, pPresenter);
        pOutState.putLong(KEY_PRESENTER_ID, id);
    }

    @SuppressWarnings("unchecked")
    public <P extends BasePresenter> P restorePresenter(Bundle pSavedInstanceState) {
        Long id = pSavedInstanceState.getLong(KEY_PRESENTER_ID);
        P presenter = (P) mPresenters.getIfPresent(id);
        mPresenters.invalidate(id);
        return presenter;
    }
}
