package ru.ddnproger.www.wylsacom.adapter;

import android.view.View;

import java.util.List;

import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.ui.viewholder.AutoViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.BaseViewHolder;

/**
 * Created by DDN on 07.04.2017.
 */

public class AutosAdapter extends BaseRecyclerViewAdapter<List<Auto>, BaseViewHolder> {

    public AutosAdapter(List<Auto> pItem) {
        super(pItem);
    }

    @Override
    protected int getConvertViewId() {
        return R.layout.view_auto_list_item;
    }

    @Override
    protected BaseViewHolder createViewHolder(View pView, int pType) {
        return new AutoViewHolder(pView);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.fill(item().get(position));
    }

    @Override
    public int getItemCount() {
        return item().size();
    }
}
