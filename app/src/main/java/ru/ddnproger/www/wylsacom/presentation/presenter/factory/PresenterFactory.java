package ru.ddnproger.www.wylsacom.presentation.presenter.factory;

import android.util.Log;

import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.ArticleDetailPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.ArticlesPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.AutoDetailPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.AutosPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.BasePresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.InstaPostsPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.MainPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.NewsDetailPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.NewsesPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.ReviewDetailPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.ReviewsPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.presenter.impl.YoutubeVideosPresenterImpl;
import ru.ddnproger.www.wylsacom.presentation.view.ArticleDetailView;
import ru.ddnproger.www.wylsacom.presentation.view.ArticlesView;
import ru.ddnproger.www.wylsacom.presentation.view.AutoDetailView;
import ru.ddnproger.www.wylsacom.presentation.view.AutosView;
import ru.ddnproger.www.wylsacom.presentation.view.BaseView;
import ru.ddnproger.www.wylsacom.presentation.view.InstaPostsView;
import ru.ddnproger.www.wylsacom.presentation.view.MainView;
import ru.ddnproger.www.wylsacom.presentation.view.NewsDetailView;
import ru.ddnproger.www.wylsacom.presentation.view.NewsesView;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewDetailView;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewsView;
import ru.ddnproger.www.wylsacom.presentation.view.YoutubeVideosView;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class PresenterFactory {

    private static final String
            TAG = "Factory";

    @SuppressWarnings("unchecked")
    public static <T extends BasePresenter, V extends BaseView> T create(V view) {

        if (view instanceof MainView) return (T) new MainPresenterImpl((MainView) view);
        if (view instanceof ReviewsView) return (T) new ReviewsPresenterImpl((ReviewsView) view);
        if (view instanceof AutosView) return (T) new AutosPresenterImpl((AutosView) view);
        if (view instanceof NewsesView) return (T) new NewsesPresenterImpl((NewsesView) view);
        if (view instanceof ArticlesView) return (T) new ArticlesPresenterImpl((ArticlesView) view);
        if (view instanceof ArticleDetailView) return (T) new ArticleDetailPresenterImpl((ArticleDetailView) view);
        if (view instanceof ReviewDetailView) return (T) new ReviewDetailPresenterImpl((ReviewDetailView) view);
        if (view instanceof NewsDetailView) return (T) new NewsDetailPresenterImpl((NewsDetailView) view);
        if (view instanceof AutoDetailView) return (T) new AutoDetailPresenterImpl((AutoDetailView) view);
        if (view instanceof YoutubeVideosView) return (T) new YoutubeVideosPresenterImpl((YoutubeVideosView) view);
        if (view instanceof InstaPostsView) return (T) new InstaPostsPresenterImpl((InstaPostsView) view);

        Log.w(TAG, "ATTENTION!!! CHECK PRESENTER FACTORY");
        return (T) new BasePresenterImpl<>(view);
    }

    @SuppressWarnings("unchecked")
    public static <T extends BasePresenter, V extends BaseView> void setView(T impl, V view){
        ((BasePresenterImpl) impl).setView(view);
    }
}
