package ru.ddnproger.www.wylsacom.ui.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.View;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Callback;

import butterknife.BindView;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.model.models.YoutubeVideo;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.ui.activity.BaseActivity;

/**
 * Created by DDN on 10.04.2017.
 */

public class VideoViewHolder extends BaseViewHolder<YoutubeVideo, BasePresenter> {

    @BindView(R.id.detail_item_image)
    AppCompatImageView mVideoImage;
    @BindView(R.id.detail_item_title)
    AppCompatTextView mVideoTitle;
    @BindView(R.id.click)
    View mClick;
    @BindView(R.id.progress)
    View mProgress;

    private Context mContext;

    public VideoViewHolder(View pView, Context pContext) {
        super(pView);
        mContext = pContext;
    }

    @Override
    protected void fillHolder() {
        mProgress.setVisibility(View.VISIBLE);
        ImageUtils.load(item().getImgUrl()).error(R.drawable.logo).resize(1000, 0).into(mVideoImage, new Callback() {
            @Override
            public void onSuccess() {
                mProgress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                mProgress.setVisibility(View.GONE);
            }
        });
        mVideoTitle.setText(Html.fromHtml(item().getSnippet().getTitle()));
        mClick.setOnClickListener(getItemClick(item()));
    }

    private View.OnClickListener getItemClick(YoutubeVideo pVideo) {
        return v -> {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((BaseActivity) mContext, CONST.KEY_YOUTUBE, pVideo.getId().getVideoId());
            mContext.startActivity(intent);
        };
    }
}