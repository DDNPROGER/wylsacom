package ru.ddnproger.www.wylsacom.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import butterknife.BindView;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.ArticlesAdapter;
import ru.ddnproger.www.wylsacom.adapter.ReviewsAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.ArticlesPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ArticlesView;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.View.VISIBLE;

/**
 * Created by DDN on 07.04.2017.
 */

public class ArticlesActivity extends BaseRecyclerActivity<Article, ArticlesAdapter, ArticlesPresenter> implements ArticlesView {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_articles;
    }

    @Override
    protected int getTitleId() {
        return R.string.articles;
    }

    @Override
    protected ArticlesAdapter getAdapter(List<Article> pItems) {
        return new ArticlesAdapter(pItems);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.articles_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
