package ru.ddnproger.www.wylsacom.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import butterknife.BindView;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.InstaPost;
import ru.ddnproger.www.model.models.YoutubeVideo;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.ArticlesAdapter;
import ru.ddnproger.www.wylsacom.adapter.InstaPostsAdapter;
import ru.ddnproger.www.wylsacom.adapter.VideoAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.ArticlesPresenter;
import ru.ddnproger.www.wylsacom.presentation.presenter.InstaPostsPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ArticlesView;
import ru.ddnproger.www.wylsacom.presentation.view.InstaPostsView;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.View.VISIBLE;

/**
 * Created by Dmitry on 10.04.2017.
 */

public class InstaPostsActivity extends BaseRecyclerActivity<InstaPost, InstaPostsAdapter, InstaPostsPresenter> implements InstaPostsView{

    @Override
    protected int getLayoutId() {
        return R.layout.activity_insta_posts;
    }

    @Override
    protected int getTitleId() {
        return R.string.insta;
    }

    @Override
    protected InstaPostsAdapter getAdapter(List<InstaPost> pItems) {
        return new InstaPostsAdapter(pItems, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.insta_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        RecyclerView.LayoutManager result;
        if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            result = new LinearLayoutManager(this);
        } else result = new GridLayoutManager(this, 3);

        return result;
    }
}
