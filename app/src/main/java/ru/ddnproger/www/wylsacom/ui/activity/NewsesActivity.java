package ru.ddnproger.www.wylsacom.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.adapter.NewsesAdapter;
import ru.ddnproger.www.wylsacom.presentation.presenter.NewsesPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.NewsesView;

/**
 * Created by DDN on 07.04.2017.
 */

public class NewsesActivity extends BaseRecyclerActivity<News, NewsesAdapter, NewsesPresenter> implements NewsesView {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_newses;
    }

    @Override
    protected int getTitleId() {
        return R.string.news;
    }

    @Override
    protected NewsesAdapter getAdapter(List<News> pItems) {
        return new NewsesAdapter(pItems);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.newses_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
