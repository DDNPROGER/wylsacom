package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.News;

/**
 * Created by DDN on 08.04.2017.
 */

public interface NewsDetailView extends ErrorView {

    void showNewsDetail(News pNews);
    void showNewsDescription(News pNews);
    void showLoading();
    void hideLoading();
}
