package ru.ddnproger.www.wylsacom;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import ru.ddnproger.www.common.UtilsManager;
import ru.ddnproger.www.model.ModelManager;

/**
 * Created by Dmitry on 07.04.2017.
 */

public class WylsaApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Context context = getApplicationContext();

        UtilsManager.onCreate(context);
        ModelManager.onCreate(context);
    }
}
