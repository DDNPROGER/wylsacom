package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.Article;

/**
 * Created by DDN on 08.04.2017.
 */

public interface ArticleDetailView extends ErrorView {

    void showArticleDetail(Article pArticle);
    void showArticleDescription(Article pArticle);
    void showLoading();
    void hideLoading();
}
