package ru.ddnproger.www.wylsacom.presentation.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Dmitry on 06.04.2017.
 */

public interface BasePresenter {

    void onCreate(@Nullable Bundle pArguments);
    void onResume();
    void onPause();
    void onStop();
    void onRestore();
    void onDestroy();
}
