package ru.ddnproger.www.wylsacom.ui.viewholder;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;
import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.presentation.presenter.factory.PresenterFactory;
import ru.ddnproger.www.wylsacom.presentation.view.BaseView;

public abstract class BaseViewHolder<T, P extends BasePresenter> extends RecyclerView.ViewHolder implements BaseView {

    private T mItem;

    private P mPr;

    BaseViewHolder(View pView) {
        super(pView);

        ButterKnife.bind(this, pView);

        mPr = PresenterFactory.create(this);
        mPr.onCreate(null);
    }

    @Override
    public void show(Context pContext, Bundle pArguments, Pair<View, String>... pPairs) {

    }

    @Override
    public void show(Context pContext, Bundle pArguments) {

    }

    @Override
    public void hide() {

    }

    @Override
    public void show() {

    }

    @Override
    public void close() {

    }

    public final void fill(T pItem) {
        mItem = pItem;
        fillHolder();
    }

    public T item() {
        return mItem;
    }

    protected View view() {
        return itemView;
    }

    protected P pr() {
        return mPr;
    }

    protected abstract void fillHolder();
}
