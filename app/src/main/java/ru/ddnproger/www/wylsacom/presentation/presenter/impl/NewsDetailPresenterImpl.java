package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.NewsDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.NewsDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class NewsDetailPresenterImpl extends BasePresenterImpl<NewsDetailView> implements NewsDetailPresenter {

    private News mNews;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public NewsDetailPresenterImpl(NewsDetailView pView) {
        super(pView);
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {
        if (pArguments.containsKey(CONST.EXTRA_NEWS)) {
            mNews = (News) pArguments.getSerializable(CONST.EXTRA_NEWS);
            view().showNewsDetail(mNews);
            loadNewsDetail();
        } else {
            view().showNewsDetail(mNews);
            view().showNewsDescription(mNews);
        }
    }

    @Override
    public void loadNewsDetail() {
        view().showLoading();
        mDisposable.add(DataProvider.getInstance().getNews(mNews.getUrl())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pNews -> {
                    mNews.setAuthor(pNews.getAuthor());
                    mNews.setAuthorLink(pNews.getAuthorLink());
                    mNews.setDescription(pNews.getDescription());
                    view().hideLoading();
                    view().showNewsDescription(mNews);
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(mNews.getUrl());
    }
}
