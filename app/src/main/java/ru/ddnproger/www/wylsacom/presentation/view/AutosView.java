package ru.ddnproger.www.wylsacom.presentation.view;

import java.util.List;

import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Review;

/**
 * Created by DDN on 07.04.2017.
 */

public interface AutosView extends BaseRecyclerView<Auto> {

}
