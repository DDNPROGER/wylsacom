package ru.ddnproger.www.wylsacom.ui.activity;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.ArticleDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ArticleDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class ArticleDetailActivity extends BaseViewActivity<ArticleDetailPresenter> implements ArticleDetailView {

    @BindView(R.id.detail_item_title)
    AppCompatTextView mTitle;
    @BindView(R.id.detail_item_image)
    AppCompatImageView mImageView;
    @BindView(R.id.detail_item_content)
    HtmlTextView mContent;
    @BindView(R.id.detail_item_author)
    AppCompatTextView mArticleAuthor;
    @BindView(R.id.loading)
    View mLoadingView;
    @BindView(R.id.detail_item_linear_author)
    View mAuthorView;

    @Override
    protected void onSetup() {
        super.onSetup();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showArticleDetail(Article pArticle) {
        mTitle.setText(Html.fromHtml(pArticle.getTitle()));
        ImageUtils.load(pArticle.getImgUrl()).resize(1000, 0).into(mImageView);
    }

    @Override
    public void showArticleDescription(Article pArticle) {
        mArticleAuthor.setText(Html.fromHtml(pArticle.getFullAuthor()));
        mContent.setHtml(pArticle.getDescription(), new HtmlHttpImageGetter(mContent, "", true));
        mAuthorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_article_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.articles_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
