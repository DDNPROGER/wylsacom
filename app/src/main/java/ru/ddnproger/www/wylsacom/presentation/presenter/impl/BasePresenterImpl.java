package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.presentation.view.BaseView;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class BasePresenterImpl<V extends BaseView> implements BasePresenter {

    private V mView;

    public BasePresenterImpl(V pView) {
        mView = pView;
    }

    protected V view() {
        return mView;
    }

    public void setView(V pView) {
        mView = pView;
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onRestore() {

    }

    @Override
    public void onDestroy() {

    }
}
