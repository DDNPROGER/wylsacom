package ru.ddnproger.www.wylsacom.adapter;

import android.content.Context;
import android.view.View;

import java.util.List;

import ru.ddnproger.www.model.models.InstaPost;
import ru.ddnproger.www.model.models.YoutubeVideo;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.ui.viewholder.BaseViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.InstaPostViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.VideoViewHolder;

/**
 * Created by DDN on 10.04.2017.
 */

public class InstaPostsAdapter extends BaseRecyclerViewAdapter<List<InstaPost>, BaseViewHolder> {

    private Context mContext;

    public InstaPostsAdapter(List<InstaPost> pItem, Context pContext) {
        super(pItem);
        mContext = pContext;
    }

    @Override
    protected int getConvertViewId() {
        return R.layout.view_insta_post_list_item;
    }

    @Override
    protected BaseViewHolder createViewHolder(View pView, int pType) {
        return new InstaPostViewHolder(pView, mContext);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.fill(item().get(position));
    }

    @Override
    public int getItemCount() {
        return item().size();
    }
}