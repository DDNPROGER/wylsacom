package ru.ddnproger.www.wylsacom.ui.activity;

import butterknife.OnClick;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.MainPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.MainView;
import ru.ddnproger.www.wylsacom.ui.factory.ViewFactory;

import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.ARTICLES_VIEW;
import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.AUTOS_VIEW;
import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.INSTA_POSTS_VIEW;
import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.NEWSES_VIEW;
import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.REVIEWS_VIEW;
import static ru.ddnproger.www.wylsacom.ui.factory.ViewFactory.YOUTUBE_VIEW;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class MainActivity extends BaseViewActivity<MainPresenter> implements MainView {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.button_insta)
    void showInstaPosts() {
        ViewFactory.create(INSTA_POSTS_VIEW).show(this, null);
    }

    @OnClick(R.id.button_youtube)
    void showYoutubeVideos() {
        ViewFactory.create(YOUTUBE_VIEW).show(this, null);
    }

    @OnClick(R.id.button_reviews)
    void showReviewsActivity() {
        ViewFactory.create(REVIEWS_VIEW).show(this, null);
    }

    @OnClick(R.id.button_articles)
    void showArticlesActivity() {
        ViewFactory.create(ARTICLES_VIEW).show(this, null);
    }

    @OnClick(R.id.button_auto)
    void showAutosActivity() {
        ViewFactory.create(AUTOS_VIEW).show(this, null);
    }

    @OnClick(R.id.button_news)
    void showNewsesActivity() {
        ViewFactory.create(NEWSES_VIEW).show(this, null);
    }
}
