package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.InstaPost;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.extension.EndlessRecyclerViewScrollListener;
import ru.ddnproger.www.wylsacom.presentation.presenter.InstaPostsPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.InstaPostsView;

/**
 * Created by DDN on 10.04.2017.
 */

public class InstaPostsPresenterImpl extends BaseRecyclerViewPresenterImpl implements InstaPostsPresenter {

    private String mLastPostId = "";

    public InstaPostsPresenterImpl(InstaPostsView pView) {
        super(pView);
    }

    @Override
    public void refresh() {
        mLastPostId = "";
        loadItems();
    }

    @Override
    public void loadItems() {
        if (mLastPostId.isEmpty()) view().showLoading();
        mCompositeDisposable.add(DataProvider.getInstance().getInstaPosts(mLastPostId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pPosts -> {
                    mItems.addAll(pPosts.getItems());
                    mLastPostId = pPosts.getLastId();
                    view().hideLoading();
                    view().showItems(mItems, pPosts.getItems().size());
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    view().showItems(mItems, 0);
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(CONST.WYLSA_INSTA_URL);
    }
}
