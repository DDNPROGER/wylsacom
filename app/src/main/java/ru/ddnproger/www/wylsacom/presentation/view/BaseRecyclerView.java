package ru.ddnproger.www.wylsacom.presentation.view;

import java.util.List;

/**
 * Created by DDN on 10.04.2017.
 */

public interface BaseRecyclerView<T extends Object> extends ErrorView {

    void restore(List<T> pItems);

    void showItems(List<T> pItems, int count);

    void showLoading();

    void hideLoading();
}
