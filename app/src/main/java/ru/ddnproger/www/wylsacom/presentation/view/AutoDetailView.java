package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Auto;

/**
 * Created by DDN on 08.04.2017.
 */

public interface AutoDetailView extends ErrorView {

    void showAutoDetail(Auto pAuto);
    void showAutoDescription(Auto pAuto);
    void showLoading();
    void hideLoading();
}
