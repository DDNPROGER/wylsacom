package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import ru.ddnproger.www.wylsacom.extension.EndlessRecyclerViewScrollListener;
import ru.ddnproger.www.wylsacom.presentation.presenter.BaseRecyclerPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.BaseRecyclerView;

/**
 * Created by DDN on 10.04.2017.
 */

public abstract class BaseRecyclerViewPresenterImpl extends BasePresenterImpl<BaseRecyclerView> implements BaseRecyclerPresenter {

    protected List<Object> mItems = new ArrayList<>();
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private EndlessRecyclerViewScrollListener mListener;

    public BaseRecyclerViewPresenterImpl(BaseRecyclerView pView) {
        super(pView);
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {
        if (pArguments == null) loadItems();
        else view().restore(mItems);
    }

    @Override
    public EndlessRecyclerViewScrollListener getOnScrollListener(RecyclerView.LayoutManager pManager) {
        if (mListener == null) {
            mListener = new EndlessRecyclerViewScrollListener(pManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    loadItems();
                }
            };
        } else mListener.setLayoutManager(pManager);
        return mListener;
    }

    @Override
    public void onDestroy() {
        if (mCompositeDisposable != null && mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
        super.onDestroy();
    }
}
