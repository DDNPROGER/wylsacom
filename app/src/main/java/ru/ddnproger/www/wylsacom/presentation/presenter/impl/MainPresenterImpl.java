package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import io.reactivex.disposables.CompositeDisposable;
import ru.ddnproger.www.wylsacom.presentation.presenter.MainPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.MainView;

/**
 * Created by Dmitry on 06.04.2017.
 */

public class MainPresenterImpl extends BasePresenterImpl<MainView> implements MainPresenter {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public MainPresenterImpl(MainView pView) {
        super(pView);
    }

    @Override
    public void onDestroy() {
        if (mCompositeDisposable != null && mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
        super.onDestroy();
    }
}
