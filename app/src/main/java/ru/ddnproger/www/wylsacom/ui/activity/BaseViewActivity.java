package ru.ddnproger.www.wylsacom.ui.activity;

/**
 * Created by Dmitry on 06.04.2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;

import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.presentation.presenter.factory.PresenterFactory;
import ru.ddnproger.www.wylsacom.presentation.presenter.manager.PresenterManager;
import ru.ddnproger.www.wylsacom.presentation.view.ErrorView;


public abstract class BaseViewActivity<T extends BasePresenter> extends BaseActivity implements ErrorView {

    private T mPr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mPr = PresenterFactory.create(this);
        } else {
            mPr = PresenterManager.getInstance().restorePresenter(savedInstanceState);
            if (mPr == null) mPr = PresenterFactory.create(this);
            else PresenterFactory.setView(mPr, this);
        }

        onSetup();

        if (mPr != null) {
            if (savedInstanceState == null) {
                mPr.onCreate(getIntent().getExtras());
            } else mPr.onCreate(savedInstanceState);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        PresenterManager.getInstance().savePresenter(mPr, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        mPr.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mPr.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        mPr.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mPr.onDestroy();
        super.onDestroy();
    }

    @Override
    public void show(Context pContext, Bundle pArguments, Pair<View, String>... pPairs) {
        Intent selfCreate = new Intent(pContext, getClass());
        if (pArguments != null) selfCreate.putExtras(pArguments);
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((BaseViewActivity) pContext, pPairs);
        pContext.startActivity(selfCreate, optionsCompat.toBundle());
    }

    @Override
    public void show(Context pContext, Bundle pArguments) {
        Intent selfCreate = new Intent(pContext, getClass());
        if (pArguments != null) selfCreate.putExtras(pArguments);
        pContext.startActivity(selfCreate);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void close() {

    }

    @Override
    public void showError(@StringRes int pErrorStringId) {
        onError();
        Snackbar.make(getWindow().getDecorView().getRootView(), pErrorStringId, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(String pErrorString) {
        onError();
        Snackbar.make(getWindow().getDecorView().getRootView(), pErrorString, Snackbar.LENGTH_LONG).show();
    }

    protected void onSetup() {
    }

    protected void onError() {
    }

    protected T pr() {
        return mPr;
    }
}
