package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.YoutubeVideo;

/**
 * Created by DDN on 10.04.2017.
 */

public interface YoutubeVideosView extends BaseRecyclerView<YoutubeVideo> {

}
