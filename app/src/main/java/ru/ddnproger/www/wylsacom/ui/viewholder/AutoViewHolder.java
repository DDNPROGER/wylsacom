package ru.ddnproger.www.wylsacom.ui.viewholder;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.View;

import com.squareup.picasso.Callback;

import butterknife.BindView;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.common.UtilsManager;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;
import ru.ddnproger.www.wylsacom.ui.factory.ViewFactory;

/**
 * Created by DDN on 07.04.2017.
 */

public class AutoViewHolder extends BaseViewHolder<Auto, BasePresenter> {

    @BindView(R.id.detail_item_image)
    AppCompatImageView mAutoImage;
    @BindView(R.id.detail_item_title)
    AppCompatTextView mAutoTitle;
    @BindView(R.id.click)
    View mClick;
    @BindView(R.id.progress)
    View mProgress;

    public AutoViewHolder(View pView) {
        super(pView);
    }

    @Override
    protected void fillHolder() {
        mProgress.setVisibility(View.VISIBLE);
        ImageUtils.load(item().getImgUrl()).error(R.drawable.logo).resize(1000, 0).into(mAutoImage, new Callback() {
            @Override
            public void onSuccess() {
                mProgress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                mProgress.setVisibility(View.GONE);
            }
        });
        mAutoTitle.setText(Html.fromHtml(item().getTitle()));
        mClick.setOnClickListener(getItemClick(item()));
    }

    private View.OnClickListener getItemClick(Auto pAuto){
        return v -> {
            Bundle args = new Bundle();
            args.putSerializable(CONST.EXTRA_AUTO, pAuto);
            Pair<View, String> pairImage = Pair.create(mAutoImage, UtilsManager.getString(R.string.pair_image));
            Pair<View, String> pairTitle = Pair.create(mAutoTitle, UtilsManager.getString(R.string.pair_title));
            ViewFactory.create(ViewFactory.AUTO_DETAIL_VIEW).show(view().getContext(), args, pairImage, pairTitle);
        };
    }
}
