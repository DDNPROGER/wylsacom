package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.Auto;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.AutoDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.AutoDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class AutoDetailPresenterImpl extends BasePresenterImpl<AutoDetailView> implements AutoDetailPresenter {

    private Auto mAuto;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public AutoDetailPresenterImpl(AutoDetailView pView) {
        super(pView);
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {
        if (pArguments.containsKey(CONST.EXTRA_AUTO)) {
            mAuto = (Auto) pArguments.getSerializable(CONST.EXTRA_AUTO);
            view().showAutoDetail(mAuto);
            loadAutoDetail();
        } else {
            view().showAutoDetail(mAuto);
            view().showAutoDescription(mAuto);
        }
    }

    @Override
    public void loadAutoDetail() {
        view().showLoading();
        mDisposable.add(DataProvider.getInstance().getAuto(mAuto.getUrl())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pAuto -> {
                    mAuto.setAuthor(pAuto.getAuthor());
                    mAuto.setAuthorLink(pAuto.getAuthorLink());
                    mAuto.setDescription(pAuto.getDescription());
                    view().hideLoading();
                    view().showAutoDescription(mAuto);
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(mAuto.getUrl());
    }
}
