package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.extension.EndlessRecyclerViewScrollListener;
import ru.ddnproger.www.wylsacom.presentation.presenter.NewsesPresenter;
import ru.ddnproger.www.wylsacom.presentation.presenter.ReviewsPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.NewsesView;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewsView;

/**
 * Created by DDN on 07.04.2017.
 */

public class NewsesPresenterImpl extends BaseRecyclerViewPresenterImpl implements NewsesPresenter {

    private int mPage = 1;

    public NewsesPresenterImpl(NewsesView pView) {
        super(pView);
    }

    @Override
    public void refresh(){
        mPage = 1;
        loadItems();
    }

    @Override
    public void loadItems() {
        if (mPage == 1) view().showLoading();
        mCompositeDisposable.add(DataProvider.getInstance().getNewses(mPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pNewses -> {
                    DataProvider.getInstance().setLoadNewsesExistHttpException(false);
                    mItems.addAll(pNewses.getData());
                    mPage++;
                    view().hideLoading();
                    view().showItems(mItems, pNewses.getData().size());
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    view().showItems(mItems, 0);
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(CONST.WYLSA_NEWSES_URL);
    }
}
