package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.ReviewDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class ReviewDetailPresenterImpl extends BasePresenterImpl<ReviewDetailView> implements ReviewDetailPresenter {

    private Review mReview;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    public ReviewDetailPresenterImpl(ReviewDetailView pView) {
        super(pView);
    }

    @Override
    public void onCreate(@Nullable Bundle pArguments) {
        if (pArguments.containsKey(CONST.EXTRA_REVIEW)) {
            mReview = (Review) pArguments.getSerializable(CONST.EXTRA_REVIEW);
            view().showReviewDetail(mReview);
            loadReviewDetail();
        } else {
            view().showReviewDetail(mReview);
            view().showReviewDescription(mReview);
        }
    }

    @Override
    public void loadReviewDetail() {
        view().showLoading();
        mDisposable.add(DataProvider.getInstance().getReview(mReview.getUrl())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pReview -> {
                    mReview.setAuthor(pReview.getAuthor());
                    mReview.setAuthorLink(pReview.getAuthorLink());
                    mReview.setDescription(pReview.getDescription());
                    view().hideLoading();
                    view().showReviewDescription(mReview);
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(mReview.getUrl());
    }
}
