package ru.ddnproger.www.wylsacom.adapter;

import android.content.Context;
import android.view.View;

import java.util.List;

import ru.ddnproger.www.model.models.Article;
import ru.ddnproger.www.model.models.YoutubeVideo;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.ui.viewholder.ArticleViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.BaseViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.VideoViewHolder;

/**
 * Created by DDN on 10.04.2017.
 */

public class VideoAdapter extends BaseRecyclerViewAdapter<List<YoutubeVideo>, BaseViewHolder> {

    private Context mContext;

    public VideoAdapter(List<YoutubeVideo> pItem, Context pContext) {
        super(pItem);
        mContext = pContext;
    }

    @Override
    protected int getConvertViewId() {
        return R.layout.view_video_list_item;
    }

    @Override
    protected BaseViewHolder createViewHolder(View pView, int pType) {
        return new VideoViewHolder(pView, mContext);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.fill(item().get(position));
    }

    @Override
    public int getItemCount() {
        return item().size();
    }
}