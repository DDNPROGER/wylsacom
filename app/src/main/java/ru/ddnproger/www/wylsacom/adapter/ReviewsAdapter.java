package ru.ddnproger.www.wylsacom.adapter;

import android.view.View;

import java.util.List;

import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.ui.viewholder.BaseViewHolder;
import ru.ddnproger.www.wylsacom.ui.viewholder.ReviewViewHolder;

/**
 * Created by DDN on 07.04.2017.
 */

public class ReviewsAdapter extends BaseRecyclerViewAdapter<List<Review>, BaseViewHolder> {

    public ReviewsAdapter(List<Review> pItem) {
        super(pItem);
    }

    @Override
    protected int getConvertViewId() {
        return R.layout.view_review_list_item;
    }

    @Override
    protected BaseViewHolder createViewHolder(View pView, int pType) {
        return new ReviewViewHolder(pView);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.fill(item().get(position));
    }

    @Override
    public int getItemCount() {
        return item().size();
    }
}
