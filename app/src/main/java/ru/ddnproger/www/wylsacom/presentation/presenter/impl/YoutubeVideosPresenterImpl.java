package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.YoutubeVideosPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.YoutubeVideosView;

/**
 * Created by DDN on 10.04.2017.
 */

public class YoutubeVideosPresenterImpl extends BaseRecyclerViewPresenterImpl implements YoutubeVideosPresenter {

    private String mNextPageToken = "";

    public YoutubeVideosPresenterImpl(YoutubeVideosView pView) {
        super(pView);
    }

    @Override
    public void refresh() {
        mNextPageToken = "";
        loadItems();
    }

    @Override
    public void loadItems() {
        if (mNextPageToken.isEmpty()) view().showLoading();
        mCompositeDisposable.add(DataProvider.getInstance().getYoutubeVideos(mNextPageToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pYoutubeVideos -> {
                    mItems.addAll(pYoutubeVideos.getItems());
                    mNextPageToken = pYoutubeVideos.getNextPageToken();
                    view().hideLoading();
                    view().showItems(mItems, pYoutubeVideos.getItems().size());
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    view().showItems(mItems, 0);
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(CONST.WYLSA_YOUTUBE_URL);
    }
}
