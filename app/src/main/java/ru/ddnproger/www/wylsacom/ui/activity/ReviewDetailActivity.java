package ru.ddnproger.www.wylsacom.ui.activity;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.model.models.Review;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.ReviewDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.ReviewDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class ReviewDetailActivity extends BaseViewActivity<ReviewDetailPresenter> implements ReviewDetailView {

    @BindView(R.id.detail_item_title)
    AppCompatTextView mTitle;
    @BindView(R.id.detail_item_image)
    AppCompatImageView mImageView;
    @BindView(R.id.review_content)
    HtmlTextView mContent;
    @BindView(R.id.detail_item_author)
    AppCompatTextView mReviewAuthor;
    @BindView(R.id.loading)
    View mLoadingView;
    @BindView(R.id.detail_item_linear_author)
    View mAuthorView;

    @Override
    protected void onSetup() {
        super.onSetup();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showReviewDetail(Review pReview) {
        mTitle.setText(Html.fromHtml(pReview.getTitle()));
        ImageUtils.load(pReview.getImgUrl()).resize(1000, 0).into(mImageView);
    }

    @Override
    public void showReviewDescription(Review pReview) {
        mReviewAuthor.setText(Html.fromHtml(pReview.getFullAuthor()));
        mContent.setHtml(pReview.getDescription(), new HtmlHttpImageGetter(mContent, "", true));
        mAuthorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_review_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reviews_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
