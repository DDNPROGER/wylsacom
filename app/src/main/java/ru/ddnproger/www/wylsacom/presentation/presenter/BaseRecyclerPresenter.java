package ru.ddnproger.www.wylsacom.presentation.presenter;

import android.support.v7.widget.RecyclerView;

import ru.ddnproger.www.wylsacom.extension.EndlessRecyclerViewScrollListener;

/**
 * Created by DDN on 10.04.2017.
 */

public interface BaseRecyclerPresenter extends BasePresenter {

    void refresh();
    void loadItems();
    void openLink();
    EndlessRecyclerViewScrollListener getOnScrollListener(RecyclerView.LayoutManager pManager);
}
