package ru.ddnproger.www.wylsacom.ui.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.ddnproger.www.wylsacom.R;

/**
 * Created by Dmitry on 06.04.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Unbinder mUnbinder;

    @BindView(R.id.toolbar)
    @Nullable
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(getLayoutId());
        mUnbinder = ButterKnife.bind(this);
        setToolbar();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    private void setToolbar() {
        if (mToolbar == null) return;

        setSupportActionBar(mToolbar);
        setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected abstract
    @LayoutRes
    int getLayoutId();
}
