package ru.ddnproger.www.wylsacom.presentation.view;

import ru.ddnproger.www.model.models.Review;

/**
 * Created by DDN on 07.04.2017.
 */

public interface ReviewsView extends BaseRecyclerView<Review> {

}
