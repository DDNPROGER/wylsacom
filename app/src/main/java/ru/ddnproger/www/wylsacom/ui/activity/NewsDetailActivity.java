package ru.ddnproger.www.wylsacom.ui.activity;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.model.models.News;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.NewsDetailPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.NewsDetailView;

/**
 * Created by DDN on 08.04.2017.
 */

public class NewsDetailActivity extends BaseViewActivity<NewsDetailPresenter> implements NewsDetailView {

    @BindView(R.id.detail_item_title)
    AppCompatTextView mTitle;
    @BindView(R.id.detail_item_image)
    AppCompatImageView mImageView;
    @BindView(R.id.news_content)
    HtmlTextView mContent;
    @BindView(R.id.detail_item_author)
    AppCompatTextView mNewsAuthor;
    @BindView(R.id.loading)
    View mLoadingView;
    @BindView(R.id.detail_item_linear_author)
    View mAuthorView;

    @Override
    protected void onSetup() {
        super.onSetup();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void showNewsDetail(News pNews) {
        mTitle.setText(Html.fromHtml(pNews.getTitle()));
        ImageUtils.load(pNews.getImgUrl()).resize(1000, 0).into(mImageView);
    }

    @Override
    public void showNewsDescription(News pNews) {
        mNewsAuthor.setText(Html.fromHtml(pNews.getFullAuthor()));
        mContent.setHtml(pNews.getDescription(), new HtmlHttpImageGetter(mContent, "", true));
        mAuthorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_news_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.newses_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.open_link){
            pr().openLink();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
