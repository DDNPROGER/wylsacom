package ru.ddnproger.www.wylsacom.presentation.presenter;

/**
 * Created by DDN on 08.04.2017.
 */

public interface ArticleDetailPresenter extends BasePresenter {

    void loadArticleDetail();
    void openLink();
}
