package ru.ddnproger.www.wylsacom.ui.viewholder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.View;

import com.squareup.picasso.Callback;

import butterknife.BindView;
import ru.ddnproger.www.common.ImageUtils;
import ru.ddnproger.www.model.models.InstaPost;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.BasePresenter;

/**
 * Created by DDN on 10.04.2017.
 */

public class InstaPostViewHolder extends BaseViewHolder<InstaPost, BasePresenter> {

    @BindView(R.id.detail_item_image)
    AppCompatImageView mPostImage;
    @BindView(R.id.detail_item_title)
    AppCompatTextView mPostTitle;
    @BindView(R.id.click)
    View mClick;
    @BindView(R.id.progress)
    View mProgress;

    private Context mContext;

    public InstaPostViewHolder(View pView, Context pContext) {
        super(pView);
        mContext = pContext;
    }

    @Override
    protected void fillHolder() {
        mProgress.setVisibility(View.VISIBLE);
        ImageUtils.load(item().getImgUrl()).error(R.drawable.logo).resize(1000, 0).into(mPostImage, new Callback() {
            @Override
            public void onSuccess() {
                mProgress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                mProgress.setVisibility(View.GONE);
            }
        });
        mPostTitle.setText(Html.fromHtml(item().getPreviewText()));
        mClick.setOnClickListener(getItemClick(item()));
    }

    private View.OnClickListener getItemClick(InstaPost pPost) {
        return v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(pPost.getLink()));
            mContext.startActivity(intent);
        };
    }
}