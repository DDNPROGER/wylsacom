package ru.ddnproger.www.wylsacom.presentation.presenter.impl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import ru.ddnproger.www.common.CONST;
import ru.ddnproger.www.common.ShareUtils;
import ru.ddnproger.www.model.DataProvider;
import ru.ddnproger.www.model.providers.exception.InternetException;
import ru.ddnproger.www.wylsacom.R;
import ru.ddnproger.www.wylsacom.presentation.presenter.AutosPresenter;
import ru.ddnproger.www.wylsacom.presentation.view.AutosView;

/**
 * Created by DDN on 07.04.2017.
 */

public class AutosPresenterImpl extends BaseRecyclerViewPresenterImpl implements AutosPresenter {

    private int mPage = 1;

    public AutosPresenterImpl(AutosView pView) {
        super(pView);
    }

    @Override
    public void refresh() {
        mPage = 1;
        loadItems();
    }

    @Override
    public void loadItems() {
        if (mPage == 1) view().showLoading();
        mCompositeDisposable.add(DataProvider.getInstance().getAutos(mPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pAutos -> {
                    DataProvider.getInstance().setLoadAutosExistHttpException(false);
                    mItems.addAll(pAutos.getData());
                    mPage++;
                    view().hideLoading();
                    view().showItems(mItems, pAutos.getData().size());
                }, pThrowable -> {
                    pThrowable.printStackTrace();
                    view().hideLoading();
                    view().showItems(mItems, 0);
                    if (pThrowable instanceof InternetException) {
                        view().showError(R.string.check_internet_connection);
                    } else view().showError(R.string.error_try_again_later);
                })
        );
    }

    @Override
    public void openLink() {
        ShareUtils.openLink(CONST.WYLSA_AUTOS_URL);
    }
}
