package ru.ddnproger.www.wylsacom.presentation.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.view.View;

import java.util.List;

/**
 * Created by Dmitry on 06.04.2017.
 */

public interface BaseView {

    void show(Context pContext, Bundle pArguments, Pair<View, String>... pPairs);
    void show(Context pContext, Bundle pArguments);
    void show();
    void hide();
    void close();
}
